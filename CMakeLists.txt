cmake_minimum_required(VERSION 3.15)

project(zonte)

enable_language(Fortran)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif(NOT CMAKE_BUILD_TYPE)

set(CMAKE_INSTALL_RPATH "\$ORIGIN/../lib;${CMAKE_INSTALL_RPATH}")

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")

set(CMAKE_Fortran_MODULE_DIRECTORY "${CMAKE_BINARY_DIR}/mod")

set(TOML_INCLUDE_DIRS "${CMAKE_BINARY_DIR}/submodules/toml-f/include/")

# -------------------------------------------------------------------------------------------------------#
# User switches; use e.g. cmake -D WITH_MPI=OFF to change                                                #
# -------------------------------------------------------------------------------------------------------#

# most codes can make use of OpenMP
find_package(OpenMP REQUIRED)

# find BLAS + LAPACK
find_package(LAPACK REQUIRED)

add_subdirectory(submodules/angular_momenta)
add_subdirectory(submodules/constants)
add_subdirectory(submodules/coulomb)
add_subdirectory(submodules/dirs)
add_subdirectory(submodules/gaussianquadrature)
add_subdirectory(submodules/grids)
add_subdirectory(submodules/newtoncotes)
add_subdirectory(submodules/progressbar)
add_subdirectory(submodules/quantumnumbers)
add_subdirectory(submodules/spherical_harmonics)
add_subdirectory(submodules/toml-f)

add_library(libzonte "src/tsurff.f90" "src/zonte.f90")

target_link_libraries(libzonte
    libangular_momenta
    libconstants
    libcoulomb
    libdirs
    libgaussianquadrature
    libgrids
    libnewtoncotes
    libprogressbar
    libquantumnumbers
    libspherical_harmonics
    toml-f-lib
)

add_executable(zonte "app/main.f90")

# generate a Git version string
execute_process(COMMAND git describe --dirty --always --tags OUTPUT_VARIABLE GIT_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)

# write compiler flags
target_compile_options(zonte PUBLIC -cpp
                                    -DVERSION="${GIT_VERSION}")

target_link_libraries(zonte
    libzonte
    ${LAPACK_LIBRARIES}
    ${BLAS_LIBRARIES}
)
