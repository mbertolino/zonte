# Zonte
- tSURRF implementation
- iSURRF implementation coming up

# Building the code

## 1. Initialize the external libraries as submodules

  ````{verbatim}
  git submodule init
  git submodule update
  ````

## 2. Make a build directory where Zonte is built

  ````{verbatim}
  mkdir build
  cd build
  cmake ../
  make
  ````

  Optionally specify flags as below if you want to compile with a different compiler or modify some options
  ```
  export BLA_VENDOR=Intel10_64ilp

  cmake -D CMAKE_C_COMPILER=$(which icc) \
   -D CMAKE_CXX_COMPILER=$(which icpc) \
   -D CMAKE_Fortran_COMPILER=$(which mpiifort) \
   -D CMAKE_Fortran_FLAGS="-qmkl -i8" \
  ..
  make
  ```
  The sequence above builds the code with the Intel Fortran compiler, MKL library for BLAS and LAPACK and 8-byte integer interface. This command can be conveniently saved in a script. Note: Only `gfortran` has been tested

## 3. Add the `build/bin` directory to your path, e.g. by inserting the following line in `.bashrc`:
  ```{verbatim}
  export PATH="/path/to/zonte/build/bin:$PATH"
  ```

# Running Zonte
  Zonte needs three paths when running:
  ```{verbatim}
  --input directory, -i directory
    Input path, default ./
  --output directory, -o directory
    Output path, default ./
  --config file, -c file
    Zonte configuration path, default ./zonte.toml
  ```

  Zonte can the be run either with the default paths `./`, `./`, and `./zonte.toml`:
  ```
  zonte
  ```
  or by specifying the paths
  ```
  zonte -i /path/to/inputdir/ -o /path/to/outputdir/ -c /path/to/config/file
  ```

  Examples are found in the `examples` directory.

## Input files

### Required input files
  The required input files need to be stored in the `/path/to/inputdir/` specified when running zonte. These are:
  - **E\_ion.dat**
    
    - Contains the real and imaginary part of ion energies, The number of rows correspond to the number of final ion channels.
     - Each row has the format:
       ````{verbatim}
       # E_real E_imag
       R1 R2
       ````
       where R1 = Re(E) and R2 = Im(E), where E is the energy of the final ion. Degenerate states are input as separate ionic states
  - **data.toml**

    - Contains information on surface radius $r_c$, gauge, number of time steps, and number of quantum numbers.
  - **qn.dat**

    - A table of the quantum numbers, l m s a. Spin s and ion a are indeces from 1 and up, not physical quantities.
  - **fields.dat**

    - Seven columns with the time grid, electric field, and vector potential, in cartesian coordinates
      ```
      # t E_x E_y E_z A_x A_y A_z
      .  .   .   .   .   .   .
      .  .   .   .   .   .   .
      ```
  - **transition\_dipoles.dat**

    - two columns with the transitions dipole matrix elements between each ion channel a and b, $\langle a | x,y,z | b \rangle$ and $\langle a | p_x,p_y,p_z | b \rangle$.
      ````{verbatim}
      do k=1,3 #k = 1 (x), k = 2 (y), k = 3 (z)
         do j=1,n_ion
            do i=1,j
               write(*,*) D(i,j,k), P(i,j,k)
            enddo
         enddo
      enddo
      ````
      where $D(i,j,k) = \langle i | \mu_k | j \rangle$, and $\mu_k = x, y, z$, i.e. cartesian dipole operators and $i$, $j$ are the final ionic states
      Similarly, $P(i,j,k) = \langle i | p_k | j \rangle$ is the corresponding matrix elemented expressed with the momentum operator (used in velocity gauge), which satisfies the relation
      $$
        \langle i | \mu_k | j \rangle = \frac{\mathrm{i} \hslash}{m(E_j - E_i)} \langle i | p_k | j \rangle
      $$
  - **wf.dat**

    - The wavefunction projected on the sphere of radius $r_c$, for each channel in qn.dat. A table of `4*nchannels` rows and `nt` (number of time steps) columns,  on the format
    ```{verbatim}
      Row 1 + 4k (k = 0, ..., nchannels - 1): Real part of amplitude in channel k (all time steps)
      Row 2 + 4k (k = 0, ..., nchannels - 1): Imag part of amplitude in channel k (all time steps)
      Row 3 + 4k (k = 0, ..., nchannels - 1): Real part of derivative in channel k (all time steps)
      Row 4 + 4k (k = 0, ..., nchannels - 1): Imag part of derivative in channel k (all time steps)
    ```

### Optional input files
  - **momentum grid** (arb. name)

    - If the momentum grid is specified in the configuration file below, this needs to be provided, in the input directory, of the following structure:
    ````{verbatim}
    # k theta phi
     . . .
    ````

## Configuration file

  - **zonte.toml** (default name)

    - Contains the information on which *tasks* to run, either tsurff or isurff and the photoelectron momentum grid, either read from a file or a constructed product grid of k θ and φ.
