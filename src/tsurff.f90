module tsurff
  use zonte
  use constants
  implicit none

  private
  public :: prepare_tsurff, drive_tsurff

  double complex, allocatable :: eye(:,:)
  double precision, allocatable :: A_integral(:,:), A2_integral(:)
  double precision, allocatable :: tj(:,:,:)

  contains

  !> Prepares matrices and module variables for tsurff
  !!
  !! @param
  subroutine prepare_tsurff()
    use spherical_harmonics
    use newtoncotes, only: cumtrapz

    allocate(eye(qn%na, qn%na))
    allocate(A_integral(t%nodes,3))
    allocate(A2_integral(t%nodes))

    ! Evaluate the bessel functions
    allocate(bessel(0:qn%lmax+1,k%nodes))
    allocate(dbessel(0:qn%lmax+1,k%nodes))
    call get_bessel(bessel, dbessel, k%r)

    ! Setup spherical harmonics
    allocate(Y(0:qn%lmax+1,-qn%mmax:qn%mmax,k%nodes))
    call get_Y(Y, k, qn)

    ! Setup 3j symbols
    allocate(tj(qn%lmin:qn%lmax,qn%lmin:qn%lmax+1,qn%mmin:qn%mmax))
    call get_thrj(tj, qn)

    ! Setup identity matrix for time stepping
    eye = identityMatrix(qn%na)

    ! Calculate integrated vector potential for Volkov
    if (t%nodes /= size(A_vp,1)) then
       print *,'ERROR: inconsistent lengths of time and vector potential arrays'
       print *,t%nodes, size(A_vp,1)
       stop
    endif
    A_integral(:,1) = cumtrapz(A_vp(:,1), t)
    A_integral(:,2) = cumtrapz(A_vp(:,2), t)
    A_integral(:,3) = cumtrapz(A_vp(:,3), t)
    A2_integral = cumtrapz(A_vp(:,1)*A_vp(:,1) &
      + A_vp(:,2)*A_vp(:,2) &
      + A_vp(:,3)*A_vp(:,3), t)
  end subroutine prepare_tsurff

  !> Drives tsurff analysis
  !!
  !! @param
  subroutine drive_tsurff()
    select case(gauge)
    case default
      error stop "Unknown gauge "//gauge
    case("velocity")
      call drive_tsurff_vg()
    case("reduced velocity")
      call drive_tsurff_rvg()
    case("length")
      call drive_tsurff_lg()
    end select
  end subroutine drive_tsurff

  subroutine drive_tsurff_vg()
    use progressbar
    use grids
    double complex, allocatable :: s(:,:,:)
    double complex, allocatable :: s_old(:,:,:)
    double complex, allocatable :: evph(:,:)
    double complex :: H_ion(qn%na,qn%na), H_ion_old(qn%na,qn%na)
    integer :: it

    allocate(evph(k%nodes, qn%na))
    allocate(s(k%nodes, qn%ns, qn%na))
    allocate(s_old(k%nodes, qn%ns, qn%na))

    write(*,*) "NB: Only linear polarization along z is implemented in velocity gauge!"

    b = dcmplx(0.d0,0.d0)
    s = dcmplx(0.d0,0.d0)
    s_old = dcmplx(0.d0,0.d0)
    H_ion_old = dcmplx(0.d0,0.d0)
    do it=1,t%nodes
      ! Calculate ion dipole matrix
      H_ion = dipole_ion_coupling(A_vp(it,:), transition_dipoles)

      ! Setup integrated Volkov phase
      call get_evphase_lgvg(evph, k, t%x(it), t%x0, A_integral(it,:), A2_integral(it))

      ! Get the single-channel surface amplitude
      s = single_channel_s0(k, bessel, dbessel, Y, wf(it,:), dwf(it,:))
      s = s + single_channel_sfield(k, A_vp(it,:), bessel, Y, wf(it,:))

      ! Multiply Volkov phase
      s = mult_evph(s, evph)

      ! Time step
      call step_tsurff(b, s, s_old, H_ion, H_ion_old)

      ! Update the user
      call progress(it, t%nodes, "#", 50, "Time iteration ")
    enddo

    deallocate(A_integral)
    deallocate(evph)
    deallocate(bessel)
    deallocate(dbessel)
    deallocate(Y)
    deallocate(s)
    deallocate(s_old)
    deallocate(eye)
  end subroutine drive_tsurff_vg

  subroutine drive_tsurff_rvg()
    use progressbar
    use grids
    double complex, allocatable :: s(:,:,:)
    double complex, allocatable :: s_old(:,:,:)
    double complex, allocatable :: evph(:,:)
    double complex :: H_ion(qn%na,qn%na), H_ion_old(qn%na,qn%na)
    integer :: it

    allocate(evph(k%nodes, qn%na))
    allocate(s(k%nodes, qn%ns, qn%na))
    allocate(s_old(k%nodes, qn%ns, qn%na))

    write(*,*) "NB: Only linear polarization along z is implemented in velocity gauge!"

    b = dcmplx(0.d0,0.d0)
    s = dcmplx(0.d0,0.d0)
    s_old = dcmplx(0.d0,0.d0)
    H_ion_old = dcmplx(0.d0,0.d0)
    do it=1,t%nodes
      ! Calculate ion dipole matrix
      H_ion = dipole_ion_coupling(A_vp(it,:), transition_dipoles)

      ! Setup integrated Volkov phase
      call get_evphase_rvg(evph, k, t%x(it), t%x0, A_integral(it,:))

      ! Get the single-channel surface amplitude
      s = single_channel_s0(k, bessel, dbessel, Y, wf(it,:), dwf(it,:))
      s = s + single_channel_sfield(k, A_vp(it,:), bessel, Y, wf(it,:))

      ! Multiply Volkov phase
      s = mult_evph(s, evph)

      ! Time step
      call step_tsurff(b, s, s_old, H_ion, H_ion_old)

      ! Update the user
      call progress(it, t%nodes, "#", 50, "Time iteration ")
    enddo

    deallocate(A_integral)
    deallocate(evph)
    deallocate(bessel)
    deallocate(dbessel)
    deallocate(Y)
    deallocate(s)
    deallocate(s_old)
    deallocate(eye)
  end subroutine drive_tsurff_rvg

  !> Drives tsurff analysis in length gauge
  !!
  !! @param
  subroutine drive_tsurff_lg()
    use grids
    use progressbar
    double complex, allocatable :: s(:,:,:)
    double complex, allocatable :: s_old(:,:,:)
    double complex, allocatable :: evph(:,:)
    double complex :: H_ion(qn%na,qn%na), H_ion_old(qn%na,qn%na)
    type(sph) :: kp
    integer :: it

    allocate(evph(k%nodes, qn%na))
    allocate(s(k%nodes, qn%ns, qn%na))
    allocate(s_old(k%nodes, qn%ns, qn%na))

    b = dcmplx(0.d0,0.d0)
    s = dcmplx(0.d0,0.d0)
    s_old = dcmplx(0.d0,0.d0)
    H_ion_old = dcmplx(0.d0,0.d0)
    do it=1,t%nodes
      ! Calculate ion dipole matrix
      H_ion = dipole_ion_coupling(E_vp(it,:), transition_dipoles)

      ! Setup integrated Volkov phase
      call get_evphase_lgvg(evph, k, t%x(it), t%x0, A_integral(it,:), A2_integral(it))

      ! LG specific calls
      kp = k + A_vp(it,:)
      call get_bessel(bessel, dbessel, kp%r)
      call get_Y(Y, kp, qn)

      ! Get the single-channel surface amplitude
      s = single_channel_s0(kp, bessel, dbessel, Y, wf(it,:), dwf(it,:))

      ! Multiply Volkov phase
      s = mult_evph(s, evph)

      ! Time step
      call step_tsurff(b, s, s_old, H_ion, H_ion_old)

      ! Update the user
      call progress(it, t%nodes, "#", 50, "Time iteration ")
    enddo

    deallocate(A_integral)
    deallocate(evph)
    deallocate(bessel)
    deallocate(dbessel)
    deallocate(Y)
    deallocate(s)
    deallocate(s_old)
    deallocate(eye)
  end subroutine drive_tsurff_lg

  !> Time steps tsurff and updates the spectral coefficient b
  !!
  !! @param b spectral amplitudes to be updated
  !! @param s single-channel surface amplitude
  !! @param s_old s from last time step
  !! @param H_ion ion-channel dipole Hamiltonian
  !! @param H_ion_old H_ion from last time step
  subroutine step_tsurff(b, s, s_old, H_ion, H_ion_old)
    double complex, intent(inout) :: b(k%nodes,qn%ns,qn%na), s(k%nodes,qn%ns,qn%na), s_old(k%nodes,qn%ns,qn%na)
    double complex, intent(inout) :: H_ion(qn%na,qn%na), H_ion_old(qn%na,qn%na)
    double complex :: H(qn%na,qn%na)
    integer :: ie, is

    do ie=1,k%nodes
      do is=1,qn%ns
        b(ie,is,:) = matmul((eye - H_ion_old), b(ie,is,:)) + 0.5d0*i_*t%dx*(s(ie,is,:) + s_old(ie,is,:))
        H = eye + H_ion

        call solve_linsys_LU(H, b(ie,is,:))

        ! Update
        s_old(ie,is,:) = s(ie,is,:)
        H_ion_old = H_ion
      enddo
    enddo
  end subroutine step_tsurff

  !> Computes the single-channel matrix element
  !! s_a(k,t) = Σ_q α_a^q(t) <k|[t + v^(int), θ]|q>
  !! in Eq.() in Ref.[1].
  !!
  !! @param k photoelectron momentum
  !! @param t time to evaluate
  !! @param besselk list of Bessel function evaluated at k
  !! @param dbesselk list of derivatives of besselk
  !! @param Ythph list of spherical harmonics Y sampled at theta and phi
  !! @param wft wavefunction for all ionization channels sampled at t
  !! @param dwft derivative of wft
  function single_channel_s0(k, bessel, dbessel, Y, wft, dwft) result(s0)
    use grids, only: sph
    type(sph), intent(in) :: k
    double precision, intent(in) :: bessel(qn%lmin:qn%lmax+1,k%nodes), dbessel(qn%lmin:qn%lmax+1,k%nodes)
    double complex, intent(in) :: Y(0:qn%lmax+1,-qn%mmax:qn%mmax,k%nodes)
    double complex, intent(in) :: wft(:), dwft(:)
    double complex :: s0(k%nodes,qn%ns,qn%na)
    integer :: ic, ie
    s0 = dcmplx(0.d0, 0.d0)

    do ie=1,k%nodes
      do ic=1,qn%nc
        s0(ie,qn%s(ic),qn%a(ic)) = s0(ie,qn%s(ic),qn%a(ic)) + get_s0_l(qn%l(ic), k%r(ie), bessel(qn%l(ic),ie), &
          dbessel(qn%l(ic),ie), Y(qn%l(ic),qn%m(ic),ie), wft(ic), dwft(ic))
      enddo
    enddo
  end function single_channel_s0

  !> Computes the field-free partial-wave single-channel surface amplitude
  !! s_{a,l}^{(0)}(k, t) defined as Eq.() in [1]
  !!
  !! @param l orbital angular momentum
  !! @param k photoelectron momentum
  !! @param besselkl lth bessel function evaluated at k times the tSURFF radius rc j_l(k rc)
  !! @param dbesselkl derivative of besselkl
  !! @param wftc wavefunction at radius rc evaluated for a specific channel c and time t
  !! @param dwftc derivative of wftc
  double complex function get_s0_l(l, k, besselkl, dbesselkl, Ylm, wftc, dwftc) result(s0_l)
    double precision :: k
    double precision :: besselkl, dbesselkl
    double complex :: Ylm
    double complex :: wftc, dwftc
    integer :: l
    double precision :: s0l_A, s0l_B
    s0l_A = k*rc*dbesselkl + besselkl
    s0l_B = rc*besselkl
    s0_l = (-i_)**l*(s0l_A*wftc - s0l_B*dwftc)*Ylm
  end function get_s0_l

  !> Computes the single-channel matrix element
  !! s_a(k,t) = Σ_q α_a^q(t) <k|[t + v^(int), θ]|q>
  !! in Eq.() in Ref.[1].
  !!
  !! @param k photoelectron momentum
  !! @param theta ejection angle of photoelectron
  !! @param A vector potential
  !! @param t time
  function single_channel_sfield(k, At, bessel, Y, wft) result(sfield)
    use grids, only: sph
    type(sph) :: k
    double precision, intent(in) :: bessel(qn%lmin:qn%lmax+1,k%nodes)
    double complex, intent(in) :: Y(0:qn%lmax+1,-qn%mmax:qn%mmax,k%nodes)
    double complex, intent(in) :: wft(:)
    double precision, intent(in) :: At(3)
    double complex :: sfield(k%nodes, qn%ns,qn%na)
    integer :: ic, ie
    sfield = dcmplx(0.d0, 0.d0)

    do ie=1,k%nodes
      do ic=1,qn%nc
        sfield(ie,qn%s(ic),qn%a(ic)) = sfield(ie,qn%s(ic),qn%a(ic)) &
          + get_sfield_lm(qn%l(ic), qn%m(ic), bessel(:,ie), Y(:,:,ie), wft(ic), At)
      enddo
    enddo
  end function single_channel_sfield

  !> Multiplies the single-channel matrix element with the Volkov phase
  !!
  !! @param s_e single-channel matrix element
  function mult_evph(s, evph) result(s_evph)
    double complex, intent(in) :: s(k%nodes,qn%ns,qn%na), evph(k%nodes,qn%na)
    double complex :: s_evph(k%nodes,qn%ns,qn%na)
    integer :: is, ie, ia
    do ie=1,k%nodes
      do is=1,qn%ns
        do ia=1,qn%na
          s_evph(ie,is,ia) = s(ie,is,ia)*evph(ie,ia)/dsqrt(2.d0*pi_)
        enddo
      enddo
    enddo
  end function mult_evph

  !> Computes the field-partial-wave single-channel surface ampltiude
  !! s_{a,l}^{(field)}(k, t) defined as Eq.() in [1]
  !!
  !! @param l orbital angular momentum
  !! @param k photoelectron momentum
  !! @param besselk list of bessel functions evaluated at k times the tSURFF radisu rc J_{}(k rc)
  !! @param Ythph list of spherical harmonics evaluated at θ and φ
  !! @param wftc wavefunction at radius rc evaluated for a specific channel c and time t
  !! @param dwftc derivative of wftc
  !! @param At vector potential evaluated at time t
  double complex function get_sfield_lm(l, m, besselk, Ythph, wftc, At) result(sfield)
    double precision, intent(in) :: besselk(qn%lmin:qn%lmax+1)
    double complex, intent(in) :: Ythph(0:qn%lmax+1,-qn%mmax:qn%mmax)
    double precision :: At(3)
    double complex :: wftc
    integer :: l, m
    sfield = -2.d0*i_*rc*At(3)*wftc*dsqrt(real(2*l+1,8))*(-1.d0)**m*(sfield_aux(l+1) + sfield_aux(l-1))
    contains
      double complex function sfield_aux(lp)
        integer :: lp
        if (lp >= 0) then
          sfield_aux = (-i_)**lp*besselk(lp)*Ythph(lp,m)*dsqrt(real(2*lp+1,8))*tj(l,lp,m)*tj(l,lp,0)
        else
          sfield_aux = 0.d0
        endif
      end function sfield_aux
  end function get_sfield_lm

  !> Returns the velocity Volkov phase used in all gauges
  !!
  !! @param evph Volkov phase
  !! @param k photoelectron momentum grid
  !! @param theta emission angle θ
  !! @param t upper time of the Volkov action integral
  !! @param t0 lower time of the Volkov action integral
  !! @param A_integral integral of the vector potential from t0 to t
  !! @param ie electron index
  subroutine get_evphase_rvg(evph, k, t, t0, A_integral)
    use grids
    double complex, intent(inout) :: evph(:,:)
    type(sph), intent(in) :: k
    double precision, intent(in) :: t, t0, A_integral(3)
    integer :: ia

    do ia=1,qn%na
      evph(:,ia) = zexp(i_*(0.5d0*k%r**2.d0 - E_a(ia))*(t - t0))*zexp(i_*(k .dot. A_integral))
    enddo
  end subroutine get_evphase_rvg
  subroutine get_evphase_lgvg(evph, k, t, t0, A_integral, A2_integral)
    use grids
    double complex, intent(inout) :: evph(:,:)
    type(sph), intent(in) :: k
    double precision, intent(in) :: t, t0, A_integral(3), A2_integral
    integer :: ia

    do ia=1,qn%na
      evph(:,ia) = zexp(i_*(0.5d0*k%r**2.d0 - E_a(ia))*(t - t0))*zexp(i_*(k .dot. A_integral)) &
        *zexp(0.5d0*i_*A2_integral)
    enddo
  end subroutine get_evphase_lgvg

  !> Solves a linear system Ax=b by LU factorization
  !!
  !! @param A system matrix
  !! @param b right hand side (overwritten by x)
  subroutine solve_linsys_LU(A, b)
    implicit none
    double complex, intent(in) :: A(:,:)
    double complex, intent(inout) :: b(:)
    integer :: n, lda = 1
    integer :: info
    integer, allocatable :: ipiv(:)
    double complex, allocatable :: btemp(:)

    n=size(A,1)
    allocate(ipiv(n))
    call zgetrf(n, n, A, n, ipiv, info)
    if ((info > 0) .or. (info < 0)) write(*,*) "Ojsan: ", info
    allocate(btemp,source=b)
    call zgetrs('N', n, lda, A, n, ipiv, btemp, n, info)
    if ((info > 0) .or. (info < 0)) write(*,*) "Ajsan: ", info
    b = btemp
    deallocate(ipiv)
  end subroutine solve_linsys_LU

  !> Returns an array of spherical harmonics Ylm(θ,φ)
  !!
  !! @param Ylm array of spherical harmonics
  !! @param theta polar angle
  !! @param phi azimuthal angle
  !! @param qn_k quantum numbers struct to hold l and m
  subroutine get_Y(Ylm, s, qn_k)
    use spherical_harmonics
    use quantumnumbers
    use grids
    type(quantnum), intent(in) :: qn_k
    type(sph), intent(in) :: s
    double complex, intent(inout) :: Ylm(0:qn_k%lmax+1,-qn_k%mmax:qn_k%mmax,s%nodes)
    integer :: ie
    do ie=1,s%nodes
      call sphharm(Ylm(:,:,ie), s%theta(ie), s%phi(ie), qn%lmax+1, qn%mmax)
    enddo
  end subroutine get_Y

  !> Returns an array of 3j symbols
  !!
  !! @param tj array of 3j symbols
  !! @param qn quantum numbers struct to hold l and m
  subroutine get_thrj(tj, qn)
    use quantumnumbers
    use angular_momenta, only: thrj
    type(QuantNum) :: qn
    double precision :: tj(qn%lmin:qn%lmax,qn%lmin:qn%lmax+1,qn%mmin:qn%mmax)
    integer :: l, m, lp
    do l=qn%lmin,qn%lmax
      do lp=qn%lmin,qn%lmax+1
        do m=qn%mmin,qn%mmax
          tj(l,lp,m) = thrj(l,lp,m)
        enddo
      enddo
    enddo
  end subroutine get_thrj

  !> Computes the dipole ionic coupling in the tSURFF eqm
  !! using p dot A in velocity gauge, and r dot E in length gauge
  !!
  !! @param M vector potential A in VG, electric field E in lg
  !! @param td dipole matrix element in respective gauge
  function dipole_ion_coupling(M, td) result(H_ion)
    double precision :: M(3)
    double complex :: td(qn%na,qn%na,3)
    double complex :: H_ion(qn%na,qn%na)
    H_ion = 0.5d0*i_*t%dx*(M(1)*td(:,:,1) + M(2)*td(:,:,2) + M(3)*td(:,:,3))
  end function dipole_ion_coupling
end module tsurff
