module zonte
  use constants
  use quantumnumbers
  use grids
  use, intrinsic :: iso_fortran_env, only : stdin=>input_unit, stdout=>output_unit, stderr=>error_unit
  implicit none

  private
  public :: t, k, E_a, A_vp, E_vp, qn, b, tasklist, gauge
  public :: bessel, dbessel, Y, wf, dwf, rc, transition_dipoles
  public :: get_bessel, identityMatrix
  public :: read_input, read_zonte_config, write_b

  !> Abstract base class for all simulation driver configurations
  type, abstract :: driver_config
  end type driver_config

  !> Configuration for the tsurff driver
  type, extends(driver_config) :: tsurff_config
  end type tsurff_config

  !> Configuration for the isurff driver
  type, extends(driver_config) :: isurff_config
    double precision :: tol_gmres !< Tolerance for gmres
  end type isurff_config

  !> Configuration of a single simulation task
  type :: task_config
    character(len=:), allocatable :: label !> Label to identify the task
    class(driver_config), allocatable :: config !> Driver configuration
  end type task_config

  ! Configurations for each method
  type(task_config), allocatable :: tasklist(:)

  ! Time
  type(Lingrid) :: t

  ! Momentum
  type(sph) :: k

  ! Field
  double precision, allocatable :: A_vp(:,:), E_vp(:,:)

  ! Wavefunction at surface
  double complex, allocatable :: wf(:,:), dwf(:,:)
  double precision :: rc

  ! Bessel functions
  double precision, allocatable :: bessel(:,:), dbessel(:,:)

  ! Angular momenta
  double complex, allocatable :: Y(:,:,:)

  ! dipole couplings of ionization channels
  double complex, allocatable :: transition_dipoles(:,:,:)

  ! Quantum numbers
  type(QuantNum) :: qn

  ! Amplitudes
  double complex, allocatable :: b(:,:,:)

  ! Ion nergies
  double complex, allocatable :: E_a(:)

  ! Gauge
  character(len=:), allocatable :: gauge

contains

  !> Calculates arrays of spherical bessel functions and the derivative
  !! evaluated at j_l(r_c*k)
  !!
  !! @param bessel spherical bessel functions
  !! @param dbessel derivative of the spherical bessel functions
  !! @param kp array of momenta
  subroutine get_bessel(bessel, dbessel, kp)
    use coulomb
    double precision, intent(inout) :: bessel(0:qn%lmax+1,k%nodes)
    double precision, intent(inout) :: dbessel(0:qn%lmax+1,k%nodes)
    double precision, intent(in) :: kp(k%nodes)
    integer :: ik, lmaxp1, dbllmaxp1
    lmaxp1 = qn%lmax+1
    dbllmaxp1 = 2*lmaxp1+1
    do ik=1,k%nodes
      call sph_bes_table(bessel(:,ik), qn%lmax+1, kp(ik)*rc)
      call dsph_bes_table(dbessel(:,ik), bessel(:,ik), qn%lmax+1, kp(ik)*rc)
    enddo
  end subroutine get_bessel

  !> Returns a complex-valued identity matrix
  !!
  !! @param n size of the identity matrix
  function identityMatrix(n) result(I)
    integer, intent(in) :: n
    double complex :: I(n,n)
    integer :: j
    I = dcmplx(0.d0,0.d0)
    do j=1,n
      I(j,j) = dcmplx(1.d0,0.d0)
    enddo
  end function identityMatrix

  !> Projects spectral amplitudes b(k,θ) onto partial waves
  !!
  !! @param bpw spectral amplitudes projected onto partial waves
  !! @param b angle-resolved spectral amplitudes
  !! @param theta azimuthal-angle grid
  !! @param phi polar-angle grid
  !! @param qn quantum numbers
  ! subroutine project_Y(bpw, b, Y, qn)
  !   use newtoncotes, only: trapz_extended
  !   double complex, allocatable :: bpw(:,:,:,:)
  !   double complex, allocatable :: b(:,:,:,:)
  !   type(Gqgrid) :: theta
  !   type(Lingrid) :: phi
  !   double complex, allocatable :: Y(:,:,:,:)
  !   type(QuantNum) :: qn
  !   double complex, allocatable :: b_temp(:)
  !   integer :: ik, ith, iph, is, ia, l, m

  !   allocate(b_temp(phi%nodes))
  !   do ia=1,qn%na
  !     do ik=1,k%nodes
  !       do l=qn%lmin,qn%lmax
  !         do m=qn%mmin,qn%mmax
  !           if (abs(m) > l) cycle

  !           b_temp = dcmplx(0.d0, 0.d0)
  !           do iph=1,phi%nodes
  !             do ith=1,theta%nodes
  !               b_temp(iph) = b_temp(iph) &
  !                 + dsin(theta%x(ith))*conjg(Y(l,m,ith,iph))*theta%w(ith)*b(ik,ith,iph,ia) &
  !                 *(theta%xN-theta%x0)/2.d0
  !             enddo
  !           enddo
  !           bpw(ik,ia,l,m) = bpw(ik,ia,l,m) + trapz_extended(b_temp, phi)
  !         enddo
  !       enddo
  !     enddo
  !   enddo
  !   deallocate(b_temp)
  ! end subroutine project_Y

  !> Reads tsurff parameters from file
  !!
  !! @param path path to file where parameters are stored
  subroutine read_zonte_config(inputpath, zonteconfigpath)
    character(len=*), intent(in) :: inputpath, zonteconfigpath

    ! Read tasks that zonte should perform
    call read_tasks(zonteconfigpath)

    ! Setup grids
    call read_sph_momentum(inputpath, zonteconfigpath)
  end subroutine read_zonte_config

  !> Reads tasks that Zonte will perform
  !!
  !! @param path to Zonte config file
  !! @param tasks list of tasks to be performed
  subroutine read_tasks(path)
    use tomlf
    character(len=*), intent(in) :: path
    type(toml_table), allocatable :: table

    integer :: itask
    type(toml_array), pointer :: arr
    type(toml_table), pointer :: child
    type(toml_context) :: context
    type(toml_error), allocatable :: error

    integer :: iu

    call safe_open(path, iu)
    call toml_load(table, iu, context=context, error=error)
    close(iu)

    if (.not. allocated(table)) then
      write(stderr, '(3a)') 'Error: Parsing failed of file: ', path, '.'
      stop
    endif

    call get_value(table, "tasks", arr)
    if (len(arr) <=0) then
      write(stderr,'(3a)') 'Missing [[tasks]] in ', path, '.'
      stop 1
    endif

    allocate(tasklist(len(arr)))
    do itask=1,size(tasklist)
      call get_value(arr, itask, child)
      call read_task(child, tasklist(itask), context)
    end do
  end subroutine read_tasks

  !> Reads a single task configuration
  !!
  !! @param table TOML table that contains the list of tasks
  !! @param task task that zonte should run
  !! @param context TOML context for input file validation
  subroutine read_task(table, task, context)
    use tomlf, only: toml_table, toml_context, get_value
    type(toml_table), intent(inout) :: table
    type(task_config), intent(out) :: task
    type(toml_context), intent(in) :: context

    type(toml_table), pointer :: child
    integer :: stat, origin

    call get_value(table, "name", task%label, stat=stat, origin=origin)
    call report_error(task%label .eq. "tsurff" &
      & .or. task%label .eq. "isurff", context, stat, origin, &
      & "Unknown task 'name'. Must be 'tsurff' or 'isurff'", &
      & "must be 'tsurff' or 'isurff'")

    select case(task%label)
    case default
      write(stderr, '(3a)') 'Error: Unknown simulation type: ', task%label, ' in task configuration'
      write(stderr, '(1a)') 'Currently: name = "tsurff" or "isurff" accepted.'
      stop
    case("tsurff")
      block
        type(tsurff_config), allocatable :: tmp
        allocate(tmp)
        call get_value(table, "config", child)
        call read_tsurff_config(child, tmp)
        call move_alloc(tmp, task%config)
      end block
    case("isurff")
      block
        type(isurff_config), allocatable :: tmp
        allocate(tmp)
        call get_value(table, "config", child)
        call read_isurff_config(child, tmp)
        call move_alloc(tmp, task%config)
      end block
    end select
  end subroutine read_task

  !> Read configuration for tsurff
  !!
  !! @param table TOML table that contains tSURFF configurations
  !! @param config tSURFF configurations
  subroutine read_tsurff_config(table, config)
    use tomlf, only: toml_table
    type(toml_table), intent(inout) :: table
    type(tsurff_config), intent(out) :: config
  end subroutine read_tsurff_config

  !> Read configuration for isurff
  !!
  !! @param table TOML table that contains iSURFF configurations
  !! @param config iSURFF configurations
  subroutine read_isurff_config(table, config)
    use tomlf
    type(toml_table), intent(inout) :: table
    type(isurff_config), intent(out) :: config
    call get_value(table, "tolerance", config%tol_gmres)
  end subroutine read_isurff_config

  !> Reads wavefunction (value and derivative), field, grids, etc from input.
  !!
  !! @param path input file path
  subroutine read_input(path)
    character(len=*), intent(in) :: path
    logical :: read_qn_header

    read_qn_header = .false.

    ! Read from data
    call read_surface(path//'data.toml')

    ! Setup field
    call read_field(path//'fields.dat', A_vp, E_vp)

    ! Setup quantum number labels
    if (qn%nc <= 0) read_qn_header = .true.
    call qn%read_qn(path//'qn.dat', read_qn_header)

    ! Read wavefunction and derivative
    call read_wf(path//'wf.dat', wf, dwf, t)

    ! Read ion energies
    call read_E_ion(path)

    ! Read dipole-transition matrix elements
    call read_transition_dipoles(path//'transition_dipoles.dat', transition_dipoles)
  end subroutine read_input

  !> Reads field from file
  !!
  !! @param path input file path
  !! @param t time grid
  !! @param A vector potential x, y, z
  !! @param E Electric field x, y ,z
  subroutine read_field(path, A, E)
    character(len=*), intent(in) :: path
    double precision, allocatable, intent(out) :: A(:,:), E(:,:)
    integer :: it
    integer :: iu

    ! Read time, A field and E field
    allocate(t%x(t%nodes))
    allocate(A(t%nodes, 3), E(t%nodes, 3))
    call safe_open(path, iu)
    call skip_comments(iu)
    do it=1,t%nodes
      read(iu,*) t%x(it), E(it,1), E(it,2), E(it,3), &
        A(it,1), A(it,2), A(it,3)
    enddo
    close(iu)
    t%x0 = t%x(1)
    t%xN = t%x(t%nodes)
    t%dx = t%x(2) - t%x(1)
  end subroutine read_field

  !> Reads in surface data from path
  !!
  !! @param path input file path
  subroutine read_surface(path)
    use tomlf, only: toml_table, toml_load, toml_context, toml_error, get_value
    character(len=*), intent(in) :: path
    type(toml_table), allocatable :: table
    type(toml_table), pointer :: child
    type(toml_context) :: context
    type(toml_error), allocatable :: error
    integer :: stat, origin
    integer :: iu

    call safe_open(path, iu)
    call toml_load(table, iu, context=context, error=error)
    close(iu)

    if (.not. allocated(table)) then
      write(stderr, '(3a)') 'Error: Parsing failed of file:', path, '.'
      stop
    endif

    ! Read in surface data
    call get_value(table, 'surface', child)
    if (associated(child)) then
      call get_value(child, 'rc', rc, stat=stat, origin=origin)
      call report_error(rc > 0, context, stat, origin, &
        & "Surface radius rc must be positive", "must be positive")

      call get_value(child, 'gauge', gauge, stat=stat, origin=origin)
      call report_error(gauge .eq. 'velocity' .or. gauge .eq. 'length' &
        & .or. gauge .eq. 'reduced velocity', context, stat, origin, &
        & "Gauge must be 'velocity' or 'length'", "'velocity' or 'length'")

      call get_value(child, 'nt', t%nodes, stat=stat, origin=origin)
      call report_error(t%nodes > 0, context, stat, origin, &
        & "Number of time steps must be positive", "must be positive")
    endif

    ! Read channel data and report errors
    call get_value(table, 'channels', child)
    if (associated(child)) then
      call get_value(child, 'nc', qn%nc, stat=stat, origin=origin)
      call report_error(qn%nc > 0, context, stat, origin, &
        & "Number of channels nc must be positive", "must be positive")

      call get_value(child, 'nl', qn%nl, stat=stat, origin=origin)
      call report_error(qn%nl > 0, context, stat, origin, &
        & "Number of electron orbital angular momenta nl must be positive", "must be positive")

      call get_value(child, 'nm', qn%nm, stat=stat, origin=origin)
      call report_error(qn%nm > 0, context, stat, origin, &
        & "Number of electron magnetic quantum number nm must be positive", "must be positive")

      call get_value(child, 'ns', qn%ns, stat=stat, origin=origin)
      call report_error(qn%ns > 0, context, stat, origin, &
        & "Number of electron spin quantum number ns must be positive", "must be positive")

      call get_value(child, 'na', qn%na, stat=stat, origin=origin)
      call report_error(qn%na > 0, context, stat, origin, &
        & "Number of ion channels na must be positive", "must be positive")
    endif
  end subroutine read_surface

  !> Read surface terms and derivative together with populations of HF and virtual
  !!
  !! @param path path to wavefunctions at surface
  !! @param wf radial wavefunction at surface
  !! @param dwf radial derivative of wavefunction at surface
  !! @param t time grid
  subroutine read_wf(path, wf, dwf, t)
    use grids
    character(len=*), intent(in) :: path
    double complex, allocatable, intent(inout) :: wf(:,:)
    double complex, allocatable, intent(inout) :: dwf(:,:)
    type(Lingrid), intent(in) :: t
    double precision, allocatable :: surfrinput(:)
    double precision, allocatable :: surfiInput(:)
    integer :: it, ic
    integer :: iu

    ! Read waves
    allocate(wf(t%nodes,qn%nc))
    allocate(dwf(t%nodes,qn%nc))
    allocate(surfrinput(t%nodes))
    allocate(surfiInput(t%nodes))

    ! Read from surf file
    call safe_open(path, iu)
    call skip_comments(iu)

    do ic=1,qn%nc
      ! Wavefunction
      read(iu,*) (surfrinput(it), it=1,t%nodes)
      read(iu,*) (surfiInput(it), it=1,t%nodes)
      wf(:,ic) = dcmplx(surfrInput, surfiInput)

      ! Derivative of wavefunction
      read(iu,*) (surfrinput(it), it=1,t%nodes)
      read(iu,*) (surfiInput(it), it=1,t%nodes)
      dwf(:,ic) = dcmplx(surfrInput, surfiInput)
    enddo
    deallocate(surfrinput)
    deallocate(surfiInput)
    close(iu)
  end subroutine read_wf

  !> Reads the ion energies
  !!
  !! @param path the directory where the ion energies are stored
  subroutine read_E_ion(path)
    double precision :: EReal, EImag
    integer :: ia
    character(len=*), intent(in) :: path
    integer :: iu

    ! Read energies from cissetup output
    call safe_open(path//"E_ion.dat", iu)
    call skip_comments(iu)
    allocate(E_a(qn%na))
    do ia=1,qn%na
      read(iu,*) EReal, EImag
      E_a(ia) = dcmplx(EReal, EImag)
    enddo
    close(iu)
  end subroutine read_E_ion

  !> Reads dipole-transition matrix between ionization channels
  !!
  !! @param
  subroutine read_transition_dipoles(path, transition_dipoles)
    double complex, allocatable :: transition_dipoles(:,:,:)
    double precision :: d_temp_r, d_temp_i
    integer :: ia, ib, ik
    character(len=*), intent(in) :: path
    integer :: iu

    allocate(transition_dipoles(qn%na,qn%na,3))

    ! Read transition dipole matrix elements
    call safe_open(path, iu)
    transition_dipoles = 0.d0
    do ik=1,3 ! k = 1 (x), k = 2 (y), k = 3 (z)
      do ib=1,qn%na
        do ia=1,ib
          read(iu,*) d_temp_r, d_temp_i
          transition_dipoles(ia,ib,ik) = dcmplx(d_temp_r, d_temp_i)
          transition_dipoles(ib,ia,ik) = dcmplx(d_temp_r, d_temp_i)
        enddo
      enddo
    enddo
    close(iu)
  end subroutine read_transition_dipoles

  !> Reads a momentum grid from spherical coordinates grid from a TOML file
  !! [grids]
  !! type = ...
  !!
  !! [grids.k] # k
  !! unit = ...
  !! param1 = ...
  !!
  !! [grids.theta] # theta
  !! ...
  !!
  !! [grids.phi] # phi
  !! ...
  !!
  !! @param sph_table_path table to read grid from if tabulated
  !! @param path input file path
  subroutine read_sph_momentum(sph_table_path, path)
    use tomlf
    character(len=*), intent(in) :: path
    character(len=*), optional, intent(in) :: sph_table_path
    type(toml_table), allocatable :: table
    type(grid) :: k_r, k_theta, k_phi
    character(len=:), allocatable :: gridtype

    type(toml_table), pointer :: child, cchild
    type(toml_context) :: context
    type(toml_error), allocatable :: error

    integer :: stat, origin

    logical :: file_exists
    integer :: iu, rc
    character(len=:), allocatable :: sph_table_filename
    integer :: ie, snodes

    call safe_open(path, iu)
    call toml_load(table, iu, context=context, error=error)
    close(iu)

    if (.not. allocated(table)) then
      write(stderr, '(3a)') 'Error: Parsing failed of file:', path, '.'
      stop
    endif

    ! Read spherical grid data
    call get_value(table, "momentum", child)
    call get_value(child, "type", gridtype, stat=stat, origin=origin)
    call report_error(gridtype .eq. "product" &
      & .or. gridtype .eq. "table", context, stat, origin, &
      "Unknown spherical grid type, must be 'product', or 'table'", &
      & "must be 'product', or 'table'")

    if (gridtype .eq. "product") then
      call get_value(child, "k", cchild)
      call read_momentum(cchild, k_r, context)
      call get_value(child, "theta", cchild)
      call read_theta(cchild, k_theta, context)
      call get_value(child, "phi", cchild)
      call read_phi(cchild, k_phi, context)

      ! Setup spherical grid
      k = sph(k_r%x, k_theta%x, k_phi%x)
    else if (gridtype .eq. "table") then
      call get_value(child, "path", sph_table_filename, stat=stat, origin=origin)
      if (stat .eq. -5) then
        call report_error(.false., context, stat, origin, &
          "Missing path to tabulated grid 'path'", &
          & "missing 'path'")
      endif
      call get_value(child, "nodes", snodes, stat=stat, origin=origin)
      if (stat .eq. -5) then
        call report_error(.false., context, stat, origin, &
          "Missing value 'nodes'", &
          & "missing nodes")
      else
        call report_error(snodes > 0, context, stat, origin, "Number of nodes must be positive", &
          & "must be positive")
      endif

      ! Read momentum grid from file
      call safe_open(sph_table_path//sph_table_filename, iu)
      call skip_comments(iu)

      allocate(k_r%x(snodes), k_theta%x(snodes), k_phi%x(snodes))
      do ie=1,snodes
        read(iu,*) k_r%x(ie), k_theta%x(ie), k_phi%x(ie)
      enddo

      k = init_sph_table(k_r%x, k_theta%x, k_phi%x)
    end if
  end subroutine read_sph_momentum

  !> Reads the momentum grid from TOML
  !!
  !! @param table TOML table that contains the list of tasks
  !! @param g momentum grid to be read
  !! @param context toml context for error reporting
  subroutine read_momentum(table, g, context)
    use tomlf, only: toml_table, toml_context, get_value
    type(toml_table), intent(inout) :: table
    type(grid), intent(inout) :: g
    type(toml_context), optional, intent(in) :: context
    integer :: stat, origin

    character(len=:), allocatable :: gridunit

    ! Read grid type
    call get_value(table, "unit", gridunit, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing physical 'unit', must be 'au' or 'eV'", &
        & "missing 'unit'")
    else
      call report_error(gridunit .eq. "au" &
        & .or. gridunit .eq. "eV", context, stat, origin, &
        "Unknown physical unit, must be 'au' or 'eV'", &
        & "must be 'au', or 'eV'")
    endif

    ! Read grid start value x0
    call get_value(table, "x0", g%x0, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing starting value 'x0'", &
        & "missing 'x0'")
    else
      call report_error(g%x0 > 0, context, stat, origin, &
        "Momentum grid starting point must be larger than zero", &
        & "must be 'x0' > 0")
    endif

    ! Read grid end value xN
    call get_value(table, "xN", g%xN, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing value 'xN'", &
        & "missing 'xN'")
    else
      call report_error(g%xN >= g%x0, context, stat, origin, &
        "Momentum grid ending point must be greater or equal to the starting point", &
        & "must be 'xN' > 'x0'")
    endif

    ! Read grid number of nodes
    call get_value(table, "nodes", g%nodes, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing value momentum 'nodes'", &
        & "missing 'nodes'")
    else
      call report_error(g%nodes > 0, context, stat, origin, "Number of momentum nodes must be larger than zero", &
        & "must be greater than 0")
      if (g%x0 .eq. g%xN) then
        call report_error(g%nodes .eq. 1, context, stat, origin, "If 'x0'='xN', nodes must be 1.", &
          & "must be 1")
      endif
      if (g%nodes .eq. 1) then
        call report_error(g%x0 .eq. g%xN, context, stat, origin, "If 'x0'/='xN', nodes must be greater than 1.", &
          & "must be greater than 1")
      endif
    endif

    ! Setup grid values
    if (gridunit .eq. "au") then
      g%x = linspace(g%x0, g%xN, g%nodes)
    else if(gridunit .eq. "eV") then
      g%x = sqrt(2.d0/energyau_)*sqrtspace(g%x0, g%xN, g%nodes)
    else
      write(stderr, '(3a)') 'Error: Unknown grid type "', gridunit, '".'
      write(stderr, '(a)') 'Currently: type = "linear", "sqrt" or "gaussian" accepted.'
      stop 1
    end if
  end subroutine read_momentum

  !> Reads an theta grid from a TOML file respecting θ \in [0, 180] degrees
  !!
  !! @param table TOML table that contains the list of tasks
  !! @param g theta grid to be read
  !! @param context toml context for error reporting
  subroutine read_theta(table, g, context)
    use tomlf, only: toml_table, toml_context, get_value
    type(toml_table), intent(inout) :: table
    type(grid), intent(inout) :: g
    type(toml_context), optional, intent(in) :: context
    integer :: stat, origin

    character(len=:), allocatable :: gridunit, gridtype

    ! Read grid physical unit
    call get_value(table, "unit", gridunit, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing physical 'unit', must be 'deg' or 'rad'", &
        & "missing 'unit'")
    else
      call report_error(gridunit .eq. "deg" &
        & .or. gridunit .eq. "rad", context, stat, origin, &
        "Unknown physical unit, must be 'deg' or 'rad'", &
        & "must be 'au', or 'eV'")
    endif

    ! Read grid sampling type, defaults to 'linear'
    call get_value(table, "type", gridtype, stat=stat, origin=origin)
    if (stat .eq. -5) gridtype = "linear"
    call report_error(gridtype .eq. "linear" &
      & .or. gridtype .eq. "gaussian", context, stat, origin, &
      "Unknown physical unit, must be 'linear' or 'gaussian'", &
      & "must be 'linear', or 'gaussian'")

    ! Read grid start value x0
    call get_value(table, "x0", g%x0, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing starting value 'x0'", &
        & "missing 'x0'")
    else
      call report_error(g%x0 >= 0, context, stat, origin, &
        "Zenith angle theta starting point 'x0' must be greater than 0", &
        & "must be greater or equal to zero")
    endif

    ! Read grid end value xN
    call get_value(table, "xN", g%xN, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing value 'xN'", &
        & "missing 'xN'")
    else
      call report_error((g%xN <= 360 .and. gridunit .eq. "deg") &
        & .or. (g%xN <= 2*pi_ .and. gridunit .eq. "rad"), context, stat, origin, &
        "Zenith angle theta ending point 'xN' must be less than 180 deg (π rad)", &
        & "must be less than 180 deg (π rad)")

      call report_error(g%xN >= g%x0, context, stat, origin, &
        "Zenith angle grid ending point must be greater or equal to the starting point", &
        & "must be 'xN' > 'x0'")
    endif

    ! Read grid number of nodes
    call get_value(table, "nodes", g%nodes, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing value momentum 'nodes'", &
        & "missing 'nodes'")
    else
      call report_error(g%nodes > 0, context, stat, origin, "Number of momentum nodes must be larger than zero", &
        & "must be greater than 0")
      if (g%x0 .eq. g%xN) then
        call report_error(g%nodes .eq. 1, context, stat, origin, "If 'x0'='xN', nodes must be 1.", &
          & "must be 1")
      endif
      if (g%nodes .eq. 1) then
        call report_error(g%x0 .eq. g%xN, context, stat, origin, "If 'x0'/='xN', nodes must be greater than 1.", &
          & "must be greater than 1")
      endif
    endif

    ! Setup grid values
    if (gridunit .eq. "deg") then
      if (gridtype .eq. "linear") then
        g%x = pi_/180.d0*linspace(g%x0, g%xN, g%nodes)
      else if (gridtype .eq. "gaussian") then
        g%x = gqwxspace(pi_/180.d0*g%x0, pi_/180.d0*g%xN, g%nodes)
      endif
    else if(gridunit .eq. "rad") then
      if (gridtype .eq. "linear") then
        g%x = linspace(g%x0, g%xN, g%nodes)
      else if (gridtype .eq. "gaussian") then
        g%x = gqwxspace(g%x0, g%xN, g%nodes)
      endif
    else
      write(stderr, '(3a)') 'Error: Unknown grid type "', gridunit, '".'
      write(stderr, '(a)') 'Currently: type = "deg" or "rad" accepted.'
      stop 1
    end if
  end subroutine read_theta

  !> Reads a phi grid from a TOML file respecting φ \in [0, 360] degrees
  !!
  !! @param table TOML table that contains the list of tasks
  !! @param g phi grid to be read
  !! @param context toml context for error reporting
  subroutine read_phi(table, g, context)
    use tomlf, only: toml_table, toml_context, get_value
    type(toml_table), intent(inout) :: table
    type(grid), intent(inout) :: g
    type(toml_context), optional, intent(in) :: context
    integer :: stat, origin

    character(len=:), allocatable :: gridunit, gridtype

    ! Read grid physical unit
    call get_value(table, "unit", gridunit, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing physical 'unit', must be 'deg' or 'rad'", &
        & "missing 'unit'")
    else
      call report_error(gridunit .eq. "deg" &
        & .or. gridunit .eq. "rad", context, stat, origin, &
        "Unknown physical unit, must be 'deg' or 'rad'", &
        & "must be 'au', or 'eV'")
    endif

    ! Read grid sampling type, defaults to 'linear'
    call get_value(table, "type", gridtype, stat=stat, origin=origin)
    if (stat .eq. -5) gridtype = "linear"
    call report_error(gridtype .eq. "linear" &
      & .or. gridtype .eq. "gaussian", context, stat, origin, &
      "Unknown grid type, must be 'linear' or 'gaussian'", &
      & "must be 'linear', or 'gaussian'")

    ! Read grid start value x0
    call get_value(table, "x0", g%x0, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing starting value 'x0'", &
        & "missing 'x0'")
    else
      call report_error(g%x0 >= 0, context, stat, origin, &
        "Azimuthal angle phi starting point 'x0' must be greater than 0", &
        & "missing 'x0'")
    endif

    ! Read grid end value xN
    call get_value(table, "xN", g%xN, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing value 'xN'", &
        & "missing 'xN'")
    else
      call report_error((g%xN <= 360 .and. gridunit .eq. "deg") &
        & .or. (g%xN <= 2*pi_ .and. gridunit .eq. "rad"), context, stat, origin, &
        "Azimuthal angle phi ending point 'xN' must be less than 360 deg (2π rad)", &
        & "must be less than 360 deg (2π rad)")
      call report_error(g%xN >= g%x0, context, stat, origin, &
        "Azimuthal angle grid ending point must be greater or equal to the starting point", &
        & "must be 'xN' > 'x0'")
    endif

    ! Read grid number of nodes
    call get_value(table, "nodes", g%nodes, stat=stat, origin=origin)
    if (stat .eq. -5) then
      call report_error(.false., context, stat, origin, &
        "Missing value momentum 'nodes'", &
        & "missing 'nodes'")
    else
      call report_error(g%nodes > 0, context, stat, origin, "Number of momentum nodes must be larger than zero", &
        & "must be greater than 0")
      if (g%x0 .eq. g%xN) then
        call report_error(g%nodes .eq. 1, context, stat, origin, "If 'x0'='xN', nodes must be 1.", &
          & "must be 1")
      endif
      if (g%nodes .eq. 1) then
        call report_error(g%x0 .eq. g%xN, context, stat, origin, "If 'x0'/='xN', nodes must be greater 1.", &
          & "must be greater than 1")
      endif
    endif

    ! Setup grid values
    if (gridunit .eq. "deg") then
      if (gridtype .eq. "linear") then
        g%x = pi_/180.d0*linspace(g%x0, g%xN, g%nodes)
      else if (gridtype .eq. "gaussian") then
        g%x = gqwxspace(pi_/180.d0*g%x0, pi_/180.d0*g%xN, g%nodes)
      endif
    else if(gridunit .eq. "rad") then
      if (gridtype .eq. "linear") then
        g%x = linspace(g%x0, g%xN, g%nodes)
      else if (gridtype .eq. "gaussian") then
        g%x = gqwxspace(g%x0, g%xN, g%nodes)
      endif
    else
      write(stderr, '(3a)') 'Error: Unknown grid type "', gridunit, '".'
      write(stderr, '(a)') 'Currently: type = "deg" or "rad" accepted.'
      stop 1
    end if
  end subroutine read_phi

  !> Writes spectral amplitudes to file
  !!
  !! @param path path to file to write results
  subroutine write_b(path)
    character(len=*), intent(in) :: path
    integer :: ie, is, ia

    ! Open file to save
    open(unit=2, file=path//"/spectrum.out")

    ! Write header of spectrum file
    write(2,'(a17,2a18,a5,a4,2a17)') "k", "θ", "φ", "spin", "ion", "breal", "bimag"

    ! Write spectral amplitues
    do ia=1,qn%na
      do ie=1,k%nodes
        do is=1,qn%ns
          write(2,'(ES17.9,ES17.9,ES17.9,I5,I4,ES17.9,ES17.9)') &
            k%r(ie), k%theta(ie), k%phi(ie), is, ia, real(b(ie,is,ia)), aimag(b(ie,is,ia))
            ! 0.5d0*k%r(ie)**2.d0*energyau_, k%theta(ie), k%phi(ie), is, ia, real(b(ie,is,ia)), aimag(b(ie,is,ia))
        enddo
      enddo
    enddo
    close(2)
  end subroutine write_b

  !> Skips a comment when reading a file
  !!
  !! @param iu file unit to read from
  subroutine skip_comments(iu)
    integer, intent(in) :: iu
    character(len=1) :: firstChar
    firstChar = '#'
    do while (firstChar .eq. '#')
      read(iu,'(A)') firstChar
    enddo
    backspace(iu)
  end subroutine skip_comments

  !> Opens a file for reading and check for errors
  !!
  !! @param
  subroutine safe_open(path, iu)
    character(len=*), intent(in) :: path
    integer, intent(inout) :: iu

    logical :: file_exists
    integer :: rc_status

    inquire(file=path, exist=file_exists)
    if (.not. file_exists) then
      write (stderr, '(3a)') 'Error: File "', path, '" not found.'
      stop
    endif

    open(action='read', file=path, iostat=rc_status, newunit=iu)
    if (rc_status /= 0) then
      write(stderr, '(3a)') 'Error: Reading file "', path, '" failed.'
      stop
    endif
  end subroutine safe_open

  !> Reports an error when reading a toml file if a logical expression
  !!
  !! @param expr logical expression to be fulfilled
  !! @param context scope of input file that is tested
  !! @param stat status of read
  !! @param origin where in the input file to report
  !! @param text1 error message
  !! @param text2 specific error message
  subroutine report_error(expr, context, stat, origin, text1, text2)
    use tomlf, only: toml_context, get_value
    logical, intent(in) :: expr
    type(toml_context) :: context
    integer :: stat, origin
    character(len=*) :: text1, text2
    if (.not. expr) then
      write(stderr, '(a)') context%report(text1, &
        & origin, text2)
      stop 1
    end if
  end subroutine report_error
end module zonte
