program main
  use zonte
  use tsurff
  use dirs
  use grids
  use quantumnumbers
  use constants
  use, intrinsic :: iso_fortran_env, only : stdin=>input_unit, stdout=>output_unit, stderr=>error_unit
  implicit none

  character(:), allocatable :: inputpath, outputpath, zonteconfigpath, outputDir
  logical :: dryrun_mode
  logical :: quiet_mode
  integer :: itask

  ! Default values
  inputpath='./'
  outputpath='./'
  zonteconfigpath='./zonte.toml'
  dryrun_mode = .false.
  quiet_mode = .false.

  ! Read command-line arguments
  call read_cli_args()

  ! Read tsurff configuration and input files
  call read_input(inputpath)
  call read_zonte_config(inputpath, zonteconfigpath)

  ! Write information on the simulation
  if (.not. quiet_mode) call write_info(stdout)

  ! Prepare spectral amplitudes
  allocate(b(k%nodes,qn%ns,qn%na))
  b = dcmplx(0.d0, 0.d0)

  ! Run surface methods
  do itask=1,size(tasklist)
    select case(tasklist(itask)%label)
    case default
      error stop "Unknown simulation type in task configuration"
    case("tsurff")
      block
        ! Run tsurff
        call prepare_tsurff()
        call drive_tsurff()
      end block
    case("isurff")
      block
        ! Run isurff
        write(stderr, '(3a)') 'Surface task "', tasklist(itask)%label, '" not implemented.'
        ! stop
      end block
    end select
  enddo

  if (dryrun_mode .eqv. .false.) then
    ! Create new directory to store the output
    call mkdirResults(outputDir, trim(adjustl(outputpath)))

    ! Copy input files to output
    call copy_files()

    ! Save info to output directory
    call write_log(outputDir)

    ! Store results
    call write_b(outputDir)
  endif

  ! Deallocate
  deallocate(b)

  contains

    !> Reads command-line arguments
    subroutine read_cli_args()
      integer :: i
      character(len=99) :: arg

      i=1
      do while (i.le.command_argument_count())
        call get_command_argument(i, arg)

        select case (arg)
        case ('-i', '--input')
           i=i+1
           call get_command_argument(i, arg)
           inputpath = trim(adjustl(arg))
           if (inputpath(len(inputpath):len(inputpath)) .ne. "/") then
             inputpath = inputpath//"/"
           endif
        case ('-o', '--output')
           i=i+1
           call get_command_argument(i, arg)
           outputpath = trim(adjustl(arg))
           if (outputpath(len(outputpath):len(outputpath)) .ne. "/") then
             outputpath = outputpath//"/"
           endif
        case ('-c', '--config')
           i=i+1
           call get_command_argument(i, arg)
           zonteconfigpath = trim(adjustl(arg))
        case ('-n', '--dry-run')
           call get_command_argument(i, arg)
           dryrun_mode = .true.
           write(*,'(a)') "Dry run, nothing will be saved."
           write(*,*)
        case ('-q', '--quiet')
           call get_command_argument(i, arg)
           quiet_mode = .true.
        case ('-v', '--version')
#if defined(VERSION)
           write(*,'(A,A)') "Git version: ", VERSION
#else
           write(*,'(A)') "Git version: unknown"
#endif
           stop
        case ('-h', '--help')
           call print_help()
           stop
        case default
           write(stderr, '(a,a,/)') 'Unrecognized command-line option: ', arg
           write(stderr, '(a)') 'Try zonte --help'
           stop
        end select
        i=i+1
      end do
    end subroutine read_cli_args

    subroutine print_help()
      print '(a)', 'usage: ./zonte  [--input path/to/inputdir/] [--output path/to/outputdir/] &
        & [--config path/to/config] [--help] [--version]'
      print '(a)',
      print '(a)', 'A program to simulate electron fluxes at a surface: time-dependent surface flux [1] (tsurff), &
        & and infinite-time surface flux [2] (isurff) methods.'
      print '(a)', 'The implementations of tsurff and isurff follow the thesis of Mattias Bertolino [3].'
      print '(a)',
      print '(a)', 'References:'
      print '(a)', '[1] L. Tao and A. Scrinzi, Photo-Electron Momentum Spectra from Minimal Volumes: &
        & The Time-Dependent Surface Flux Method, '
      print '(a)', '    New J. Phys. 14, 013021 (2012).'
      print '(a)', '[2] F. Morales, T. Bredtmann, and S. Patchkovskii, ISURF: A Family of Infinite-Time Surface Flux Methods, '
      print '(a)', '    J.  Phys. B: At. Mol. Opt. Phys. 49, 245001 (2016).'
      print '(a)', '[3] M. Bertolino, Applications of Time-Dependent Configuration-Interaction Singles for Photoelectrons in &
      & Attosecond and Free-Electron Laser Sciences,'
      print '(a)', '    Doktorsavhandling (sammanläggning), Department of Physics, Lund University, 2023.'
      print '(a)',
      print '(a)', 'Optional switches:'
      print '(a)', '   --input directory, -i directory'
      print '(a)', '    Input path, default ./'
      print '(a)', '   --output directory, -o directory'
      print '(a)', '    Output path, default ./'
      print '(a)', '   --config file, -c file'
      print '(a)', '    Zonte configuration path, default ./zonte.toml'
      print '(a)', '   --dry-run, -n'
      print '(a)', '    Perform a trial run without saving'
      print '(a)', '   --quiet, -q'
      print '(a)', '    Do not print information on screen'
      print '(a)', '   --help, -h'
      print '(a)', '    Print this help message'
      print '(a)', '   --version, -v'
      print '(a)', '    Print version'
    end subroutine print_help

    !> Write info
    subroutine write_info(u)
      integer, intent(in) :: u

      write(u,'(a6,a30)') "Date: ", datetime()
#if defined(VERSION)
      write(u,'(A,A)') "Git version: ", VERSION
#else
      write(u,'(A)') "Git version: unknown"
#endif

      write(u,*)
      write(u,'(2a)') "Input path: ", inputpath
      write(u,'(2a)') "zonte config path: ", zonteconfigpath
      write(u,'(2a)') "Output path: ", outputpath
      write(u,*)

      write(u,'(a)') "Time grid:"
      call t%writeGridInfo("t", u)
      ! write(u,*) "Momentum grid:"
      ! call k%writeGridInfo("x")
      ! write(u,*) "Theta:"
      ! call theta%writeGridInfo("t")
      ! write(u,*) "Phi:"
      ! call phi%writeGridInfo("f")

      write(u,'(a)') "Quantum numbers:"
      call qn%write_qn(u)

      write(u,*)
    end subroutine write_info

    !> Write information on the simulation in a log file
    !!
    !! @param path path which directory to write log
    subroutine write_log(path)
      character(len=*), intent(in) :: path

      open(unit=4, file=path//"info.log")
      call write_info(4)
      close(4)
    end subroutine

    !> Copy input files to output directory
    subroutine copy_files()
      call copyFile(inputpath//"data.toml", outputDir)
      call copyFile(inputpath//"E_ion.dat", outputDir)
      call copyFile(inputpath//"fields.dat", outputDir)
      call copyFile(zonteconfigpath, outputDir)
    end subroutine copy_files

    !> Returns current date and time
    function datetime()
      implicit none
      integer(8) :: i
      character(len=30) :: datetime
      i = time8()
      call ctime(i, datetime)
    end function datetime

end program main
